package com.egolden.simple;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.egolden.wangdenghui.okhttpcodeanalysis.R;

import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OriginalActivity extends AppCompatActivity {

    private static final String tag = OriginalActivity.class.getSimpleName();
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_original);
        textView = (TextView) findViewById(R.id.id_textview);
    }


    public void getMethod(View view) {
        String url = "http://mobile.weather.com.cn/data/forecast/101010100.html?_=1381891660081";
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Log.d(tag, call.toString());

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                Log.d(tag, response.body().toString());
                Log.d(tag, response.message());
                final String result = response.body().string();
                Log.d(tag, result);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(result);
                    }
                });
            }
        });


    }


    public void postString(View view) {
        OkHttpClient client = new OkHttpClient.Builder().build();
        RequestBody requestBody = new MultipartBody.Builder()
                .addFormDataPart("key", "b9a58d775049f11efc97b5ba9f07a9a6")
                .addFormDataPart("dtype", "")
                .build();
        Request request = new Request.Builder()
                .url("http://op.juhe.cn/onebox/basketball/team")
                .post(requestBody)
                .tag(this)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(tag, call.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String result = response.body().string();
                Log.d(tag, response.message());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(result);
                    }
                });
            }
        });
    }

    private final OkHttpClient client = getDefaultClient();

    private OkHttpClient getDefaultClient() {
    Cache cache = new Cache(Environment.getExternalStorageDirectory(), 10 * 1024 * 1024);
        OkHttpClient client = new OkHttpClient.Builder().cache(cache).build();
        return client;
    }

    /**
     * 测试网络缓存
     *
     * @param view
     */
    public void cache(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cacheExcute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void cacheExcute() throws Exception {
        Request request = new Request.Builder()
                .url("http://publicobject.com/helloworld.txt")
                .build();
        Response response1 = client.newCall(request).execute();
        if (!response1.isSuccessful()) throw new IOException("Unexpected code " + response1);

        String response1Body = response1.body().string();
        System.out.println("Response 1 response:          " + response1);
        System.out.println("Response 1 cache response:    " + response1.cacheResponse());
        System.out.println("Response 1 network response:  " + response1.networkResponse());

        Response response2 = client.newCall(request).execute();
        if (!response2.isSuccessful()) throw new IOException("Unexpected code " + response2);

        String response2Body = response2.body().string();
        System.out.println("Response 2 response:          " + response2);
        System.out.println("Response 2 cache response:    " + response2.cacheResponse());
        System.out.println("Response 2 network response:  " + response2.networkResponse());

        System.out.println("Response 2 equals Response 1? " + response1Body.equals(response2Body));

    }

    public void postFile(View view) {

    }

    public void getUser(View view) {

    }


    public void getUsers(View view) {
    }


    public void getHttpsHtml(View view) {
        String url = "https://kyfw.12306.cn/otn/";

//                "https://12
//        url =3.125.219.144:8443/mobileConnect/MobileConnect/authLogin.action?systemid=100009&mobile=13260284063&pipe=2&reqtime=1422986580048&ispin=2";
    }

    public void getImage(View view) {
    }


    public void uploadFile(View view) {

    }


    public void multiFileUpload(View view) {
    }


    public void downloadFile(View view) {
    }


}
